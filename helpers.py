import math


def to_int(text):
    encoded = text.encode('utf-8', 'ignore')
    return int.from_bytes(encoded, 'big')


def parse_int(integer):
    length = math.ceil(integer.bit_length() / 8)
    encoded = integer.to_bytes(length, 'big')
    return encoded.decode('utf-8', 'ignore')


def string_to_vector(value, l=4):
    chunks = [value[i:i + l]
              for i in range(0, len(value), l)]

    return [to_int(chunk) for chunk in chunks]


def vector_to_string(vector, l=4):
    return ''.join([parse_int(element) for element in vector])
