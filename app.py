from flask import Flask, request, jsonify
from flask_cors import CORS
from main import encrypt, decrypt

app = Flask(__name__)
cors = CORS(app)


@app.route('/encrypt', methods=['POST'])
def get_cipher():
    data = request.get_json()['params']
    text = data['text']
    key = data['key']
    number_of_rounds = data['numberOfRounds']

    variable = encrypt(text, key, number_of_rounds)

    return jsonify(variable)


@app.route('/decrypt', methods=['POST'])
def get_deciphered_text():
    data = request.get_json()['params']
    text = data['text']
    key = data['key']
    number_of_rounds = data['numberOfRounds']

    variable = decrypt(text, key, number_of_rounds)

    return jsonify(variable)


if __name__ == "__main__":
    app.run()
