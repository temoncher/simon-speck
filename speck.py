class SpeckCipher(object):
    _block_size = 64
    _key_size = 128
    _rounds = []

    def __init__(self, key, number_of_rounds = 32):
        # Setup word size
        self.word_size = self._block_size >> 1

        # Create Properly Sized bit mask for truncating addition and left shift outputs
        self.mod_mask = (2 ** self.word_size) - 1

        # Mod mask for modular subtraction
        self.mod_mask_sub = (2 ** self.word_size)

        # Setup Circular Shift Parameters
        if self._block_size == 32:
            self.beta_shift = 2
            self.alpha_shift = 7
        else:
            self.beta_shift = 3
            self.alpha_shift = 8

        # Parse the given key and truncate it to the key length
        try:
            self.key = key & ((2 ** self._key_size) - 1)
        except (ValueError, TypeError):
            print('Invalid Key Value!')
            print('Please Provide Key as int')
            raise

        self._number_of_rounds = number_of_rounds

        # Pre-compile key schedule
        self.key_schedule = [self.key & self.mod_mask]
        l_schedule = [(self.key >> (x * self.word_size)) & self.mod_mask for x in
                      range(1, self._key_size // self.word_size)]

        for x in range(self._number_of_rounds - 1):
            new_l_k = self._encrypt_round(
                l_schedule[x], self.key_schedule[x], x)
            l_schedule.append(new_l_k[0])
            self.key_schedule.append(new_l_k[1])

    def _encrypt_round(self, x, y, k):
        rs_x = ((x << (self.word_size - self.alpha_shift)) +
                (x >> self.alpha_shift)) & self.mod_mask

        add_sxy = (rs_x + y) & self.mod_mask

        new_x = k ^ add_sxy

        ls_y = ((y >> (self.word_size - self.beta_shift)) +
                (y << self.beta_shift)) & self.mod_mask

        new_y = new_x ^ ls_y

        return new_x, new_y

    def _decrypt_round(self, x, y, k):
        xor_xy = x ^ y

        new_y = ((xor_xy << (self.word_size - self.beta_shift)) +
                 (xor_xy >> self.beta_shift)) & self.mod_mask

        xor_xk = x ^ k

        msub = ((xor_xk - new_y) + self.mod_mask_sub) % self.mod_mask_sub

        new_x = ((msub >> (self.word_size - self.alpha_shift)) +
                 (msub << self.alpha_shift)) & self.mod_mask

        return new_x, new_y

    def encrypt(self, plaintext):
        self._rounds = []

        try:
            b = (plaintext >> self.word_size) & self.mod_mask
            a = plaintext & self.mod_mask
        except TypeError:
            print('Invalid plaintext!')
            print('Please provide plaintext as int')
            raise

        b, a = self._encrypt_function(b, a)

        ciphertext = (b << self.word_size) + a

        return ciphertext, self._rounds

    def decrypt(self, ciphertext):
        self._rounds = []

        try:
            b = (ciphertext >> self.word_size) & self.mod_mask
            a = ciphertext & self.mod_mask
        except TypeError:
            print('Invalid ciphertext!')
            print('Please provide plaintext as int')
            raise

        b, a = self._decrypt_function(b, a)

        plaintext = (b << self.word_size) + a

        return plaintext, self._rounds

    def _encrypt_function(self, upper_word, lower_word):

        x = upper_word
        y = lower_word

        # Run Encryption Steps For Appropriate Number of Rounds
        for k in self.key_schedule:
            new_x, new_y = self._encrypt_round(x, y, k)

            x = new_x
            y = new_y

            plaintext = (y << self.word_size) + x
            self._rounds.append(plaintext)

        return x, y

    def _decrypt_function(self, upper_word, lower_word):

        x = upper_word
        y = lower_word

        # Run Encryption Steps For Appropriate Number of Rounds
        for k in reversed(self.key_schedule):
            new_x, new_y = self._decrypt_round(x, y, k)

            x = new_x
            y = new_y

            plaintext = (y << self.word_size) + x
            self._rounds.append(plaintext)

        return x, y
