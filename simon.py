from collections import deque


class SimonCipher(object):
    _z0 = 0b01100111000011010100100010111110110011100001101010010001011111
    _z1 = 0b01011010000110010011111011100010101101000011001001111101110001
    _z2 = 0b11001101101001111110001000010100011001001011000000111011110101
    _z3 = 0b11110000101100111001010001001000000111101001100011010111011011
    _z4 = 0b11110111001001010011000011101000000100011011010110011110001011

    _block_size = 64
    _key_size = 128
    _rounds = []
    _zseq = _z3

    def __init__(self, key, number_of_rounds = 32):
        # Setup word size
        self._word_size = self._block_size >> 1

        # Create Properly Sized bit mask for truncating addition and left shift outputs
        self._mod_mask = (2 ** self._word_size) - 1

        # Parse the given key and truncate it to the key length
        try:
            self.key = key & ((2 ** self._key_size) - 1)
        except (ValueError, TypeError):
            print('Invalid Key Value!')
            print('Please Provide Key as int')
            raise

        self._number_of_rounds = number_of_rounds

        self._generate_key_schedule()

    def encrypt(self, plaintext):
        self._rounds = []

        try:
            b = (plaintext >> self._word_size) & self._mod_mask
            a = plaintext & self._mod_mask
        except TypeError:
            print('Invalid plaintext!')
            print('Please provide plaintext as int')
            raise

        b, a = self._encrypt_function(b, a)

        ciphertext = (b << self._word_size) + a

        return ciphertext, self._rounds

    def decrypt(self, ciphertext):
        self._rounds = []

        try:
            b = (ciphertext >> self._word_size) & self._mod_mask
            a = ciphertext & self._mod_mask
        except TypeError:
            print('Invalid ciphertext!')
            print('Please provide ciphertext as int')
            raise

        a, b = self._decrypt_function(a, b)

        plaintext = (b << self._word_size) + a

        return plaintext, self._rounds

    def _encrypt_round(self, x, y, k):
        # Generate all circular shifts
        ls_1_x = ((x >> (self._word_size - 1)) + (x << 1)) & self._mod_mask
        ls_8_x = ((x >> (self._word_size - 8)) + (x << 8)) & self._mod_mask
        ls_2_x = ((x >> (self._word_size - 2)) + (x << 2)) & self._mod_mask

        # XOR Chain
        xor_1 = (ls_1_x & ls_8_x) ^ y
        xor_2 = xor_1 ^ ls_2_x

        new_y = x
        new_x = k ^ xor_2

        return new_x, new_y

    def _generate_key_schedule(self, _number_of_rounds=32):
        # Pre-compile key schedule
        m = self._key_size // self._word_size
        self.key_schedule = []

        # Create list of subwords from encryption key
        k_init = [((self.key >> (self._word_size * ((m-1) - x)))
                   & self._mod_mask) for x in range(m)]

        k_reg = deque(k_init)  # Use queue to manage key subwords

        round_constant = self._mod_mask ^ 3  # Round Constant is 0xFFFF..FC
        # Generate all round keys
        for x in range(self._number_of_rounds):
            rs_3 = ((k_reg[0] << (self._word_size - 3)) +
                    (k_reg[0] >> 3)) & self._mod_mask

            if m == 4:
                rs_3 = rs_3 ^ k_reg[2]

            rs_1 = ((rs_3 << (self._word_size - 1)) +
                    (rs_3 >> 1)) & self._mod_mask

            c_z = ((self._zseq >> (x % 62)) & 1) ^ round_constant

            new_k = c_z ^ rs_1 ^ rs_3 ^ k_reg[m - 1]

            self.key_schedule.append(k_reg.pop())
            k_reg.appendleft(new_k)

    def _encrypt_function(self, upper_word, lower_word):
        x = upper_word
        y = lower_word

        # Run Encryption Steps For Appropriate Number of Rounds
        for k in self.key_schedule:
            new_x, new_y = self._encrypt_round(x, y, k)

            x = new_x
            y = new_y

            plaintext = (y << self._word_size) + x
            self._rounds.append(plaintext)

        return x, y

    def _decrypt_function(self, upper_word, lower_word):
        x = upper_word
        y = lower_word

        # Run Encryption Steps For Appropriate Number of Rounds
        for k in reversed(self.key_schedule):
            new_x, new_y = self._encrypt_round(x, y, k)

            x = new_x
            y = new_y

            plaintext = (y << self._word_size) + x
            self._rounds.append(plaintext)

        return x, y
