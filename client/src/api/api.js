import axios from "axios";

axios.defaults.headers.common["Content-Type"] = "application/json";

const root = "http://127.0.0.1:5000/";

const encrypt = (params) => axios.post(`${root}encrypt`, { params });
const decrypt = (params) => axios.post(`${root}decrypt`, { params });

export default { encrypt, decrypt };
