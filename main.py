import helpers
from simon import SimonCipher
from speck import SpeckCipher


class EncodeDTO:
    def __init__(self, simon_cipher='', speck_cipher='', simon_vectors=[], speck_vectors=[]):
        self.simon_cipher = simon_cipher
        self.speck_cipher = speck_cipher
        self.simon_vectors = simon_vectors
        self.speck_vectors = speck_vectors


def encrypt(plaintext, key, number_of_rounds):
    key_int = helpers.to_int(key)
    vector = helpers.string_to_vector(plaintext)
    simon_rounds = []
    speck_rounds = []

    # Initialize ciphers with a key
    simon = SimonCipher(key_int, number_of_rounds)
    speck = SpeckCipher(key_int, number_of_rounds)

    simon_ciphered_vector = []
    speck_ciphered_vector = []

    for entry in vector:
        simon_ciphered_int, simon_encrypt_rounds = simon.encrypt(entry)
        speck_ciphered_int, speck_encrypt_rounds = speck.encrypt(entry)

        simon_rounds.append(simon_encrypt_rounds)
        speck_rounds.append(speck_encrypt_rounds)

        simon_ciphered_vector.append(simon_ciphered_int)
        speck_ciphered_vector.append(speck_ciphered_int)

    # print('[Simon] Ciphered vector', simon_ciphered_vector)
    # print('[Speck] Ciphered vector', speck_ciphered_vector)

    # simon_ciphered_text = helpers.vector_to_string(simon_ciphered_vector)
    # speck_ciphered_text = helpers.vector_to_string(speck_ciphered_vector)
    simon_ciphered_binary = ''.join(
        [str(bin(v)) for v in simon_ciphered_vector])
    speck_ciphered_binary = ''.join(
        [str(bin(v)) for v in speck_ciphered_vector])

    # print('[Simon] Ciphered text', simon_ciphered_text)
    # print('[Speck] Ciphered text', speck_ciphered_text)

    simon_vectors = [] * number_of_rounds
    speck_vectors = [] * number_of_rounds

    for round_number in range(number_of_rounds):
        simon_vectors.append([])
        speck_vectors.append([])
        for vector_index in range(len(vector)):
            simon_vectors[round_number].append(
                simon_rounds[vector_index][round_number])
            speck_vectors[round_number].append(
                speck_rounds[vector_index][round_number])

    simon_round_texts = [''] * number_of_rounds
    speck_round_texts = [''] * number_of_rounds

    for index in range(number_of_rounds):
        simon_round_texts[index] = helpers.vector_to_string(
            simon_vectors[index])
        speck_round_texts[index] = helpers.vector_to_string(
            speck_vectors[index])

    print(simon_ciphered_binary, speck_ciphered_binary)

    return {
        'simon': {
            'output': simon_ciphered_binary,
            'vectors': simon_vectors,
            'texts': simon_round_texts
        },
        'speck': {
            'output': speck_ciphered_binary,
            'vectors': speck_vectors,
            'texts': speck_round_texts
        }
    }


def decrypt(ciphertext, key, number_of_rounds):
    if not ciphertext:
        return {
            'simon': {
                'output': '',
            },
            'speck': {
                'output': '',
            }
        }

    vector = [int(chunk, 2) for chunk in ciphertext.split('0b') if chunk]
    key_int = helpers.to_int(key)

    # print('[Simon] Vector', vector)
    # print('[Speck] Vector', vector)

    # Initialize ciphers with a key
    simon = SimonCipher(key_int, number_of_rounds)
    speck = SpeckCipher(key_int, number_of_rounds)

    simon_deciphered_vector = []
    speck_deciphered_vector = []

    for index in range(len(vector)):
        simon_deciphered_int = simon.decrypt(vector[index])[0]
        speck_deciphered_int = speck.decrypt(vector[index])[0]

        simon_deciphered_vector.append(simon_deciphered_int)
        speck_deciphered_vector.append(speck_deciphered_int)

    # print('[Simon] Deciphered vector', simon_deciphered_vector)
    # print('[Speck] Deciphered vector', speck_deciphered_vector)

    simon_deciphered_text = helpers.vector_to_string(simon_deciphered_vector)
    speck_deciphered_text = helpers.vector_to_string(speck_deciphered_vector)

    return {
        'simon': {
            'output': simon_deciphered_text,
        },
        'speck': {
            'output': speck_deciphered_text,
        }
    }


# text = 'some little text of mine'
# print('#text', text, len(text))
# key = 'some key'
# number_of_rounds = 3

# encoded = encrypt(text, key, number_of_rounds)
# print('#encoded', encoded['simon']['cipher'], len(encoded['simon']['cipher']))
# decoded = decrypt(encoded['simon']['cipher'], key, number_of_rounds)
# print('#decoded', decoded['simon'], len(decoded['simon']))

# simon = SimonCipher(helpers.to_int(key), number_of_rounds)
# vector = helpers.string_to_vector(text)
# enc = simon.encrypt(vector[0])[0]
# print('#enc', enc)
# dec = simon.encrypt(enc)[0]
# print('#dec', dec)
# string = helpers.vector_to_string([dec])
# print('#text', text, len(text))
# print('#string', string, len(string))
